﻿using Spring.Data.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Spring.Data.Support;
using Mast.SpringDao;

namespace Mast.Spring.Dao
{
    public class SpringMastProxy
    {
        public static IDbConnection GetConnection()
        {
            IDbProvider dbProvider = DbProviderProxy.Instance.DbProvider;
            if (dbProvider == null)
                return null;

            ConnectionTxPair connectionTxPair = ConnectionUtils.GetConnectionTxPair(dbProvider);

            return connectionTxPair.Connection;
        }

        public static IDbTransaction GetTransaction()
        {
            IDbProvider dbProvider = DbProviderProxy.Instance.DbProvider;
            if (dbProvider == null)
                return null;

            ConnectionTxPair connectionTxPair = ConnectionUtils.GetConnectionTxPair(dbProvider);

            return connectionTxPair.Transaction;
        }

        public static Boolean IsSpringManager()
        {
            IDbProvider dbProvider = DbProviderProxy.Instance.DbProvider;
            return dbProvider == null ? false : true;
        }
    }
}
